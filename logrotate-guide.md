## Log Rotate Service Logs
### Create log rotate config file /var/log/sui-node.log
```bash
sudo vim /etc/logrotate.d/sui-node
```
```
/var/log/sui-node.log {
  rotate 7
  daily
  missingok
  compress
  notifempty
  copytruncate
  su root adm
}
```
### Apply log rotate config
```bash
sudo logrotate /etc/logrotate.d/sui-node
```
### Modify Sui node service to output to its own log file.
Add StandardOutput and StandardError lines in the [Service] section of your validator service file.
```bash
sudo vim /etc/systemctl/system/sui-node.service
```
```
[Unit]
Description=Sui Node

[Service]
User=sui
WorkingDirectory=/opt/sui/
Environment=RUST_BACKTRACE=1
Environment=RUST_LOG=info,sui_core=debug,narwhal=debug,narwhal-primary::helper=info,jsonrpsee=error
ExecStart=/opt/sui/bin/sui-node --config-path /opt/sui/config/validator.yaml
Restart=always

StandardOutput=append:/var/log/sui-node.log
StandardError=append:/var/log/sui-node.log

[Install]
WantedBy=multi-user.target
```
### Apply changes and restart the service
```bash
sudo systemctl daemon-reload
sudo systemctl restart sui-node
```
### Check logs from new log file
```bash
tail -f /var/log/sui-node.log
```