# DWALLET TOOLS

In this repository you can find tools and guides for dWallet network validator servers.

## Guides

- [Log Rotate Guide](https://gitlab.com/blockscope-net/dwallet-tools/-/blob/main/logrotate-guide.md)

## Tools

- [dWallet Grafana Dashboard (Narwhal)](https://gitlab.com/blockscope-net/dwallet-tools/-/blob/main/dwallet-validator-grafana-dashboard.json?ref_type=heads): Grafana dashboard for individual nodes with most important metrics taken from official Sui Grafana dashboards.

### Grafana Dashboard

This dashboard aims to provide a series of useful charts and data to monitor the health of your testnet and mainnet validator nodes. The dashboard has a select for selecting different server instances, so if you include several servers in your Prometheus config and the corresponding variables in the Grafana dashboard you will have access to inspecting all your instances in the same dashboard.

![Dashboard screenshot 1](image.png)

![Dashboard screenshot 2](image-1.png)

_Prometheus Config_

```yaml
- job_name: "sui-metrics"
  static_configs:
    - targets: ["1.1.1.1:9184"]
      labels:
        alias: dwallet-testnet-metrics
        instance: "dWallet Testnet"
    - targets: ["2.2.2.2:9184"]
      labels:
        alias: dwallet-mainnet-metrics
        instance: "dWallet Mainnet"
```

_Grafana Config_

You need the following variables to be able to access the instances you declare in your prometheus through the Host selector on top of the Grafana dashboard.

![Config vars](image-2.png)

Once declared you should see your instances in the Host selector.

![Dashboard selectors](image-3.png)

## Authors and acknowledgment

Tools and resources developed by the Blockscope team for the dWallet project.

## License

Copyright 2023 Blockscope.net

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Project status

WIP: our team will be uploading new tools and improvements so we keep help growing the dWallet ecosystem.
